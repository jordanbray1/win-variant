use std::convert::TryInto;

use winapi::shared::wtypes::{
    VT_BOOL, VT_BSTR, VT_I2, VT_I4, VT_I8, VT_INT, VT_R4, VT_R8, VT_UI2, VT_UI4, VT_UI8, VT_UINT,
};

use ndarray::ArrayD;
use serde::{
    ser::{Error as SerError, SerializeSeq},
    Serialize,
};

use crate::SafeArray;

fn serialize_array<ST, S>(arr: ArrayD<ST>, serializer: S) -> Result<S::Ok, S::Error>
where
    ST: serde::Serialize,
    S: serde::Serializer,
{
    if arr.ndim() == 1 {
        let mut seq = serializer.serialize_seq(Some(arr.len()))?;
        for e in arr.into_iter() {
            seq.serialize_element(&e)?;
        }
        return seq.end();
    }
    arr.serialize(serializer)
}

impl Serialize for SafeArray {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        match self.get_vartype().map(|vt| vt as u32) {
            Ok(VT_BOOL) => {
                let a: ArrayD<bool> = self
                    .try_into()
                    .map_err(|e| SerError::custom(format!("{:?}", e)))?;
                serialize_array(a, serializer)
            }
            Ok(VT_BSTR) => {
                let a: ArrayD<String> = self
                    .try_into()
                    .map_err(|e| SerError::custom(format!("{:?}", e)))?;
                serialize_array(a, serializer)
            }
            Ok(VT_I2) => {
                let a: ArrayD<i16> = self
                    .try_into()
                    .map_err(|e| SerError::custom(format!("{:?}", e)))?;
                serialize_array(a, serializer)
            }
            Ok(VT_I4) => {
                let a: ArrayD<i32> = self
                    .try_into()
                    .map_err(|e| SerError::custom(format!("{:?}", e)))?;
                serialize_array(a, serializer)
            }
            Ok(VT_INT) => {
                let a: ArrayD<i32> = self
                    .try_into()
                    .map_err(|e| SerError::custom(format!("{:?}", e)))?;
                serialize_array(a, serializer)
            }
            Ok(VT_I8) => {
                let a: ArrayD<i64> = self
                    .try_into()
                    .map_err(|e| SerError::custom(format!("{:?}", e)))?;
                serialize_array(a, serializer)
            }
            Ok(VT_R4) => {
                let a: ArrayD<f32> = self
                    .try_into()
                    .map_err(|e| SerError::custom(format!("{:?}", e)))?;
                serialize_array(a, serializer)
            }
            Ok(VT_R8) => {
                let a: ArrayD<f64> = self
                    .try_into()
                    .map_err(|e| SerError::custom(format!("{:?}", e)))?;
                serialize_array(a, serializer)
            }
            Ok(VT_UI2) => {
                let a: ArrayD<u16> = self
                    .try_into()
                    .map_err(|e| SerError::custom(format!("{:?}", e)))?;
                serialize_array(a, serializer)
            }
            Ok(VT_UI4) => {
                let a: ArrayD<u32> = self
                    .try_into()
                    .map_err(|e| SerError::custom(format!("{:?}", e)))?;
                serialize_array(a, serializer)
            }
            Ok(VT_UINT) => {
                let a: ArrayD<u32> = self
                    .try_into()
                    .map_err(|e| SerError::custom(format!("{:?}", e)))?;
                serialize_array(a, serializer)
            }
            Ok(VT_UI8) => {
                let a: ArrayD<u64> = self
                    .try_into()
                    .map_err(|e| SerError::custom(format!("{:?}", e)))?;
                serialize_array(a, serializer)
            }
            Ok(vt) => return Err(SerError::custom(format!("unsupported vartype {:}", vt))),
            Err(e) => return Err(SerError::custom(format!("{:?}", e))),
        }
    }
}
